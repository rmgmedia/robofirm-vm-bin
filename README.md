# Robofirm Bin Scripts
 
 These are scripts designed to help in the Robofirm development workflow and devops. This should be installed on all 
 VMs and server environments at `~/bin/robofirm`. Most of these scripts are intended to be run at the document root of
 your project. Generally all commands have a --help option.
 
## Composer
 * A copy of composer is included. You can automatically run this after cloning this repository into your bin directory.
   Composer has a self-update feature, so run `composer self-update` as needed and then commit back up. 

## Devops Scripts
 
 * `magento-push` - Deploys code, db, and/or media. 
 * `magento-pull` - Pull db or media from a source server and installs into your project. Resets base URLs, etc. on db pull. 
 * `build-magento-instance-from-branch` - Builds a new Magento instance for a given Git branch.
 * `deploy-from-tag` - Checks out a Git tag, then runs magento-push.
 * `tag-magento-release` - Checks out a Git branch named "release-x.x.x", merges it into master, tags it, then merges it back down to develop
 * `build-magento-instance-from-branch` - Builds a dedicated Magento instance like http://myproject.robofirm.net/release-1.2.3 for a given Project branch 
 * `destroy-magento-instance-from-branch` - Destroys the Magento instance built by build-magento-instance-from-branch for the given project branch
 * `set-magento-permissions` - Set file permissions to our usual setup described in [Recommended User File Permissions Setup for Magento](https://rmgmedia.atlassian.net/wiki/display/ROBO/Recommended+User+File+Permissions+Setup+for+Magento)
 * `detect-magento-root` - Helper command used by other scripts to determine the Magento document root. This travels up the directory tree looking for "app/Mage.php", "htdocs/app/Mage.php" or "magento/app/Mage.php"
 
## Development Utility Scripts
 
 * `magento-build` - Runs any post checkout install scripts needed to make a working directory operational (Composer, Git submodules, Composer), then clears the cache
 * `magento-clear-cache` (will at some point be deprecated in favor of n98 cache:flush)
 * `n98-magerun` - https://github.com/netz98/n98-magerun
 * `modman` - See https://github.com/colinmollenhour/modman
 * `clearlogs` - Clears logs relevant to Magento. 
 * `composer-redeploy` - Redeploys Magento modules from vendor into htdocs

## N98 & Modules
* n98-magerun is included and can be run with the command `n98-magerun`. n98 is included via Composer.
* n98 modules are also included. Symlink the included `.n98-magerun` directory to your home directory with 
  `cd ~; ln -s bin/robofirm/.n98-magerun;`. All n98-modules are included via Composer.

Included modules:

* Magento Project Mess Detector - https://github.com/AOEpeople/mpmd

## Git Hooks

The git-template directory includes useful git hooks that do the following:

* `post-merge` - On a `git pull` or `git merge`, this hook runs installers like `compass compile`, 
  `composer install`, and `git submodule update --init`, then clears the cache through `n98`. This
  is not run when there is a merge conflict, even after resolving the merge.
* `prepare-commit-msg` - This hook prepends the commit message with the Jira issue ID. The Jira ID is 
  obtained from the branch name prefix if the branch is recognized as a branch with this naming convention.
  Example: `git commit -m "Test commit message"` on branch `DEMO-123-example-story` produces the commit
  message `DEMO-123 Test commit message`. Without the `-m` flag, this will prepopulate the commit message 
  with the ticket number.



To enable these hooks, run this command. Note: On most current VMs, the bin script lives in `~/bin` 
instead of `~/bin/robofirm`, so you might need to substitute.
```
git config --global init.templatedir '~/bin/robofirm/git_template';
```

For existing repository checkouts, you must also run this command from the repository root:
```
git init;
```

Note: The above command resets core.filemode to true, which can make it look like all of your files are updated. 
If this happens, run the following command. If anyone can figure out hot to get core.filemode to default to false or 
unset for new repo clones or calling git init, please update this readme.
```
git config core.filemode false; git submodule foreach "git config core.filemode false"
```